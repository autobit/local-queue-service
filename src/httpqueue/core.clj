(ns httpqueue.core
  (:require
    [httpqueue.queue :as queue]
    [compojure.core :refer [defroutes GET POST]]
    [ring.adapter.jetty :refer [run-jetty]]
    [ring.util.response :as res])
  (:gen-class))

(defn read-queue [queue]
  (let [timeout 20000 ; 20 second timeout to emulate SQS long polling.
        result (queue/pop! queue timeout)]
    (if (nil? result)
      (-> (res/response nil)
          (res/status 204))
      (-> (res/response result)
          (res/status 200)
          (res/content-type "application/json")))))

(defn write-queue [queue content]
  (-> (res/response (queue/push! queue content))
      (res/status 201)
      (res/content-type "application/json")))

(def queue (queue/create))
(defroutes app
  (GET "/" [] (read-queue queue))
  (POST "/" {body :body} (write-queue queue (slurp body))))

(defn -main [& args]
  (run-jetty app {:port 9000}))
