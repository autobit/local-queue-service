(ns httpqueue.queue
  (:refer-clojure :exclude [pop!]))

(defn create
  ([]
    (new java.util.concurrent.LinkedBlockingQueue))
  ([capacity]
    (new java.util.concurrent.LinkedBlockingQueue capacity)))

(defn push! [queue item]
  (do
    (.put queue item)
    item))

(defn pop!
  ([queue]
    (.poll queue))
  ([queue timeout]
    (.poll queue timeout (java.util.concurrent.TimeUnit/MILLISECONDS))))
