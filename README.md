Local Queue Service
===
**Local Queue Service (LQS)** is meant to act as a simple development environment queue service. LQS supports 2 operations: enqueue and dequeue. It is released under the MIT license.

### Enqueue `POST`

Enqueuing is done by sending a POST request to the service. Only JSON messages are supported, so the Content-Type specified in the request header should be `application/json`.

If there is an issue storing the message in the queue, the server will reply with an `HTTP 400 Bad Request` status code. Enqueuing should only be considered successful if the client receives a `HTTP 201 Created` status code.

The response body will always match the request JSON body.

**Example Request:**
```
POST  HTTP/1.1
Host: localhost:9000
Content-Type: application/json

{
  "name": "Sam",
  "age":21
}
```

**Example Response:**
```
{
  "name": "Sam",
  "age": 21
}
```

### Dequeue `GET`

Dequeuing is done by sending a GET request to the server. Dequeuing follows the FIFO algorithm.

If the queue is empty, the server will reply with an `HTTP 204 No Content` status code, otherwise the oldest message will be taken from the queue and sent as a response with the `HTTP 200 OK` status code.

**Example Request:**
```
GET  HTTP/1.1
Host: localhost:9000
Content-Type: application/json
```

**Example Response:**
```
{
  "name": "Sam",
  "age": 21
}
```

# Project Setup

With **Leiningen** installed on the development machine, run: (note that LQS runs on port 9000 by default)

```
$ lein run
```

# Packaging & Running

To package the application as an Uber Jar, run:

```
$ lein uberjar
```

To run the application as a Jar, run:

```
$ java -jar target/uberjar/httpqueue-1.0.0-standalone.jar
```

### Notes

  - This is definitely not intended for any production use.
  - The queue is **volatile** and will not persist between sessions.
