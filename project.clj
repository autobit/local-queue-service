(defproject httpqueue "1.0.0"
  :description "A light weight, in-memory queue with push & pop implemented as POST & GET HTTP verbs."
  :url "https://autobit.ca"
  :license {:name "MIT"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.5.0"]
                 [compojure "1.5.1"]]
  :main ^:skip-aot httpqueue.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
